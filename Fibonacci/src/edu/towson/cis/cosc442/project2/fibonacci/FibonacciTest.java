package edu.towson.cis.cosc442.project2.fibonacci;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * Unit tests for {@link Fibonacci}.
 * 
 * @author Josh Dehlinger
 *
 */		
public class FibonacciTest {

	/** The fibonacci test object. */
	private Fibonacci fibonacci;
		
	/**
	 * Sets the up.
	 */
	@Before 
	/**
	 * The setUp method that creates the necessary test objects.
	 */
	public void setUp(){
		fibonacci = new Fibonacci();
	}
	
	
	/**
	 * Test fibonacci.
	 */
	@Test 
	/**
	 * Runs various equality tests against the Fibonacci class
	 */
	public void testFibonacciZero() {	
		assertEquals("0", 0, fibonacci.fibonacci(0));
	}
	
	@Test
	public void testFibonacciOne() {	
		assertEquals("1", 1, fibonacci.fibonacci(1));
	
	}
	
	@Test
	public void testFibonacciTwo() {	
		assertEquals("2", 1, fibonacci.fibonacci(2));
	}
	
	@Test
	public void testFibonacciThree() {	
		assertEquals("3", 2, fibonacci.fibonacci(3));
	}
	
	@Test
	public void testFibonacciFour() {	
		assertEquals("4", 3, fibonacci.fibonacci(4));
	}
	
	@Test
	public void testFibonaccFive() {	
		assertEquals("5", 5, fibonacci.fibonacci(5));
	}
	
	@Test
	public void testFibonacciSix() {	
		assertEquals("6", 8, fibonacci.fibonacci(6));
	}
	
	@Test
	public void testFibonacciSeven() {	
		assertEquals("7", 13, fibonacci.fibonacci(7));
	}
}
