package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineTest {
	
	VendingMachine VM;
	VendingMachineItem VMItem1, VMItem2, VMItem3, VMItem4;

	@Before
	public void setUp() throws Exception {
		VM = new VendingMachine();
		VMItem1 = new VendingMachineItem("Chocolate covered Soup", 3.25);
		VMItem2 = new VendingMachineItem("Yizzlers", 1.25);
		VMItem3 = new VendingMachineItem("Gummy Platypus", .50);
		VMItem4 = new VendingMachineItem("Dirt in a bag", 10.00);
	}

	@After
	public void tearDown() throws Exception {
	}

	
	//Adding an item to the vending machine
	@Test
	public void testAddItem() {	//Adds all items to the four slots
		VM.addItem(VMItem1, "A");
		VM.addItem(VMItem2, "B");
		VM.addItem(VMItem3, "C");
		VM.addItem(VMItem4, "D");
		
	}
	@Test(expected = VendingMachineException.class) // throws error if two items try to go in the same slot
	public void testAddItemSameSlot() {
		VM.addItem(VMItem1, "A");
		VM.addItem(VMItem2, "A");
		
	}
	@Test
	public void testAddItemSameItem() { // Test to see if the same item can go into two slots
		VM.addItem(VMItem1, "A");
		VM.addItem(VMItem1, "B");
	}
	
	@Test(expected = VendingMachineException.class)
	public void testAddItemWrongSlot() { //Test to make sure you can put an item in the wrong slot
		VM.addItem(VMItem1, "F");

	}
	
	@Test(expected = NullPointerException.class)
	public void testAddItemNull() { //Test to see if an error is thrown for null inputs
		VM.addItem(VMItem1, null);
	}
	
	
	//Removing an item from the vending machine
	@Test
	public void testRemoveItem() { // test to see if an item can be removed and replaced
		VM.addItem(VMItem1, "A");
		VM.removeItem("A");
		VM.addItem(VMItem2, "A");
	}
	
	@Test
	public void testRemoveTwoItem() { // removing two items and replacing one
		VM.addItem(VMItem1, "A");
		VM.addItem(VMItem2, "B");
		VM.removeItem("A");
		VM.removeItem("B");
		VM.addItem(VMItem2, "A");
	}
	
	@Test(expected = VendingMachineException.class)
	public void testRemoveEmptyItem() { // Throws error when trying to remove an item that isn't there
		VM.removeItem("A");
	}

	
	
	//Inserting money into the vending machine
	@Test
	public void testInsertMoney() { //adds 10 cents then a dollar then 25 cents that adds up to 1.35 
		VM.insertMoney(.10);		// and compares it with the current balance to see if they match
		VM.insertMoney(1.00);
		VM.insertMoney(.25);
		assertEquals(1.35, VM.getBalance(), 0.001);
	}
	
	@Test
	public void testInsertRandomMoney() { // adds ups money that is more then two decimals to see if an 
		VM.insertMoney(.01);			  // error is thrown	
		VM.insertMoney(1.115);
		VM.insertMoney(.222);
		assertEquals(1.347, VM.getBalance(), 0.001);
	}
	
	@Test(expected = VendingMachineException.class) //Check to see if the vending machine will catch a 
	public void testInsertNegativeMoney() {			// negative number and throws a VendingMachineException
		VM.insertMoney(-.25);
		assertEquals(0, VM.getBalance(), 0.001);
	}
	
	@Test
	public void testInsertNoMoney() { //Checks if insertMoney class can take a zero integer
		VM.insertMoney(0);
		assertEquals(0, VM.getBalance(), 0.001);
	}
	
	@Test
	public void testInsertLittleMoney() { //Checks if the insertMoney class can take a penny
		VM.insertMoney(0.01);
		assertEquals(0.01, VM.getBalance(), 0.001);
	}
	
	@Test
	public void testInsertnonDouble() { //Inserts an integer instead of a double
		VM.insertMoney(3);
		assertEquals(3, VM.getBalance(), 0.001);
	}
	

	//Getting balance
	@Test
	public void testGetBalance() { // returns the initial balance which should be zero
		assertEquals(0, VM.getBalance(), 0.001);
	}

	
	
	//Making a purchase from the vending machine
	@Test
	public void testMakePurchase() { //returns true is item and money are correct
		VM.addItem(VMItem1, "A");
		VM.insertMoney(3.25);
		assertEquals(VM.makePurchase("A"), true);
	}
	@Test(expected = VendingMachineException.class)
	public void testMakePurchaseWrongCode() {  //throws error if item is wrong
		VM.addItem(VMItem1, "A");
		VM.insertMoney(3.25);
		VM.makePurchase("F");
	}
	
	@Test
	public void testMakePurchaseWrongChange() { //returns false is the money is wrong
		VM.addItem(VMItem1, "A");
		VM.insertMoney(.01);
		assertEquals(VM.makePurchase("A"), false);
	}
	
	@Test
	public void testMakePurchaseCorrectBalance() { //Test to see if the correct balance is returned after making a purchase
		VM.addItem(VMItem1, "A");
		VM.insertMoney(10.00);
		VM.makePurchase("A");
		assertEquals(6.75, VM.returnChange(), 0.001);
	}
	@Test
	public void testMakePurchaseMultiplePurchase() { //Purchase multiple items with mistakes
		VM.addItem(VMItem1, "A"); // Add item 1
		VM.addItem(VMItem2, "B"); // Add item 2
		VM.addItem(VMItem4, "D"); // Add item 3
		VM.insertMoney(1.00);	  // Insert 1.00. Total = 1.00
		VM.makePurchase("A");	  // returns false. Not enough money
		VM.insertMoney(6.25);	  // Insert 6.25. Total = 7.25	
		VM.makePurchase("B");	  // Purchase B. Total = 6.00
		VM.insertMoney(5.00);	  // Add 5.00. Total = 11.00
		VM.makePurchase("C");	  // returns false. Slot C is empty
		VM.makePurchase("D");	  // Purchase D. Total = 1.00 		
		assertEquals(1.00, VM.returnChange(), 0.001); 
	}

	
	//Returning change
	@Test
	public void testReturnChange() { // Test to see if the balance goes back to zero once the change
		VM.insertMoney(1.25);		 // is returned
		VM.returnChange();
		assertEquals(0, VM.getBalance(), 0.001);
	}
	
	@Test
	public void testReturnCorrectChange() { //Test to see if the returned change is the correct 
		VM.insertMoney(1.25);				// amount inserted
		assertEquals(1.25, VM.returnChange(), 0.001);
	}
	

}
